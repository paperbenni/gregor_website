
const ENGLISH = 0;
const GERMAN = 1;
const PORTUGESE = 2;

var language = ENGLISH;

function setlang(lang) {
    console.log("hello world" + lang);
    elements = document.querySelectorAll("[data-de]");
    console.log(elements.length)
    for (let o = 0; o < elements.length; o++) {
        i = elements[o];

        switch (lang) {
            case GERMAN:
                if (i.hasAttribute("data-de")) {
                    i.innerHTML = i.dataset.de;
                }
                break;
            case PORTUGESE:
                if (i.hasAttribute("data-pt")) {
                    i.innerHTML = i.dataset.pt;
                }
                break;
            case ENGLISH:
                if (i.hasAttribute("data-en")) {
                    i.innerHTML = i.dataset.en;
                }
                break;
            default:
                break;
        }
    }
}
