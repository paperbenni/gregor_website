module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
      extend: {
        backgroundImage: theme => ({
         'brazil': "url('/img/brazil1.jpg')",
        })
      },
  },
  variants: {
      extend: {
          translate: ['active', 'hover']
      },
  },
    plugins: [
        require('@tailwindcss/forms')
    ],
}
